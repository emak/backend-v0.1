# README

## Development environment

Use the docker-compose.yaml in docker-dev to setup the databases for a dev environment
`cd docker-dev && docker-compose up -d`

## Build
To build the docker images use the docker-compose.yaml

`docker-compose build`

This will build two version of the app, one that launches the rails server and one that launches the sidekiq worker

This allows us to build two version and upload from the same exact image to heroku as a web app with workers on the side


## ENV variables needed in production

* SMTP_PASSWORD: SMTP server password
* DATABASE_URL: HEROKU postgress database URL
* SECRET_KEY_BASE: Secret key base hash
* RAILS_ENV: production

optional
* RAILS_LOG_TO_STDOUT: if the logs should output to the heroku LOG

## Useful commands for rails
From the JOGL/ repository (**NOT FROM JOGL/Jogl-Backend/**):
*Create databases (if it's first launch)*
```bash
docker-compose run backend rails db:create
```
*Migrate database*
```bash
docker-compose run backend rails db:migrate
```
*There are only interests seeds currently **YOUR FRONT MAY NOT WORK IF NOT SEEDED***
```bash
docker-compose run backend rails db:seed:interests
```
*Show all routes*
```bash
docker-compose run backend rails routes
```
*Open rails console*
```bash
docker-compose run backend rails c
```
## Controller architecture

Currently all api controllers are located in [app/controllers/api](https://gitlab.com/JOGL/backend-v0.1/tree/master/app/controllers/api) file.
The API uses custom [active model serializers](https://github.com/rails-api/active_model_serializers) for json rendering. There are few [concers/helpers](https://medium.freecodecamp.org/add-callbacks-to-a-concern-in-ruby-on-rails-ef1a8d26e7ab)
that are created.
Concerns are also used for [serializers](https://gitlab.com/JOGL/backend-v0.1/tree/master/app/serializers/concerns/api) which helps to centralize certain methods
that are used throughout different serializers.
The mailer passes through sidekiq to facilitate the email sending.

## Database architecture

The usage of models is relatively simple in JOGL_BACKEND for one exception.
We used for several occasions [HABTM](https://guides.rubyonrails.org/association_basics.html#choosing-between-has-many-through-and-has-and-belongs-to-many) (Has And Belong To Many) relations between
different models. The reason why we user [HABTM](https://guides.rubyonrails.org/association_basics.html#choosing-between-has-many-through-and-has-and-belongs-to-many) it's because we had several models
linked together with many to many relation but the difference between [HABTM](https://guides.rubyonrails.org/association_basics.html#choosing-between-has-many-through-and-has-and-belongs-to-many) and
has_many is that you use [HABTM](https://guides.rubyonrails.org/association_basics.html#choosing-between-has-many-through-and-has-and-belongs-to-many) only if you need the ids of each model inside the relational table between 2 models and nothing else. If you have something other
than ids inside the relational (ex: status, role etc..) you'll need to have a
many to many relation (has_many to has_many).
The best example would be User and Interest relation.
*User has many interests, interests can belong to many users.* Between these 2
models there's a many to many relation meaning that we only need to link User ids
to Interest ids. This way there's no need to create the relational model UserInterests
separately because *has_and_belongs_to_many* does the job.
