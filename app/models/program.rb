class Program < ApplicationRecord

  resourcify
  include AlgoliaSearch
  include RelationHelpers
  include Utils

  has_many :challenges

  has_many :users_programs
  has_many :users, through: :users_programs

  has_one :feed

  has_one_attached :banner
  validates :banner, content_type: ["image/png", "image/jpeg", "image/gif"]
  validates :short_title, uniqueness: true

  after_create :create_feed
  before_create :sanitize_description
  before_update :sanitize_description

  def create_feed
    feed = Feed.create({program_id: self.id, object_type:'program'})
    save
  end

end
