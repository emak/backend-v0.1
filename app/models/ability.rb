# # frozen_string_literal: true
#
# class Ability
#   include CanCan::Ability
#
#   def initialize(user)
#     # Define abilities for the passed in user here. For example:
#     #
#     #   user ||= User.new # guest user (not logged in)
#     #   if user.admin?
#     #     can :manage, :all
#     #   else
#     #     can :read, :all
#     #   end
#     #
#     # The first argument to `can` is the action you are giving the user
#     # permission to do.
#     # If you pass :manage it will apply to every action. Other common actions
#     # here are :read, :create, :update and :destroy.
#     #
#     # The second argument is the resource the user can perform the action on.
#     # If you pass :all it will apply to every resource. Otherwise pass a Ruby
#     # class of the resource.
#     #
#     # The third argument is an optional hash of conditions to further filter the
#     # objects.
#     # For example, here the user can only update published articles.
#     #
#     #   can :update, Article, :published => true
#     #
#     # See the wiki for details:
#     # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
#     can :read, Project, is_private: false
#     can :read, Post
#     can :read, Feed
#     can :read, Community, is_private: false
#     can :read, User
#
#     if user.present?
#       # can :manage Feed, user_id: user.id
#       can :manage User, id: user.id
#       can :manage Post, user_id: user.id
#
#       can :destroy, Community, :id => Community.with_role(:owner, user).pluck(:id)
#       can :destroy, Project, :id => Project.with_role(:owner, user).pluck(:id)
#
#       can :manage Community, :id => Community.with_role(:admin, user).pluck(:id)
#       can :manage Project, :id => Project.with_role(:admin, user).pluck(:id)
#
#       can :create, Post, if user.has_role?(:member)
#
#     end
#
#   end
# end
