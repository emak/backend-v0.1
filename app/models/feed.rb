class Feed < ApplicationRecord
  resourcify
  has_and_belongs_to_many :users
  has_and_belongs_to_many :projects
  has_and_belongs_to_many :community
  has_and_belongs_to_many :posts
  has_and_belongs_to_many :programs
  has_and_belongs_to_many :needs
end
