class Skill < ApplicationRecord
  include AlgoliaSearch
  has_and_belongs_to_many :users
  has_and_belongs_to_many :projects
  has_and_belongs_to_many :community
  has_and_belongs_to_many :challenge
  has_and_belongs_to_many :needs

  algoliasearch do
    attribute :skill_name
  end
end
