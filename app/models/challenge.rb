class Challenge < ApplicationRecord
  resourcify
  include RelationHelpers
  include AlgoliaSearch
  include Utils


  has_many :challenges_users
  has_many :challenges_projects
  has_many :challenges_communities
  has_many :users, through: :challenges_users
  has_many :projects, through: :challenges_projects
  has_many :communities, through: :challenges_communities
  has_one :feed
  belongs_to :program, optional: true

  has_and_belongs_to_many :skills
  has_and_belongs_to_many :interests

  has_one_attached :banner
  has_many_attached :documents
  validates :banner, content_type: ["image/png", "image/jpeg", "image/gif"]

  # TODO FIX THE VALIDATION FOR HAS_MANY

  # validates :documents, content_type: ["image/png",
  #                                      "image/jpeg",
  #                                      "image/gif",
  #                                      "text/csv",
  #                                      "application/pdf",
  #                                      "image/svg+xml",
  #                                      "application/x-tar",
  #                                      "image/tiff",
  #                                      "application/zip",
  #                                      "application/x-7z-compressed"
  #                                    ]
  # validates :documents, content_size: 31457280  # Document must be less than 30Mb

  enum status: [:draft, :soon, :active, :completed]

  after_create :create_feed
  
  before_create :sanitize_description
  before_update :sanitize_description

  algoliasearch do
    attribute :id,
              :title,
              :short_description,
              :interests,
              :skills,
              :status,
              :launch_date,
              :end_date,
              :final_date
    add_attribute :obj_type
    geoloc :latitude, :longitude
  end

  def create_feed
    feed = Feed.create({challenge_id: self.id, object_type:'challenge'})
    save
  end

end
