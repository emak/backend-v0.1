class User < ApplicationRecord
  rolify
  include AlgoliaSearch
  include RelationHelpers
  include Generators
  include Utils

            # Include default devise modules.
            devise :database_authenticatable,
                    :recoverable, :rememberable, :trackable, :validatable,
                    :confirmable, :lockable, :async
            include DeviseTokenAuth::Concerns::User
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_and_belongs_to_many :skills
  has_and_belongs_to_many :interests
  has_one :feed

  has_many :users_projects
  has_many :projects, through: :users_projects
  has_many :challenges_users
  has_many :challenges, through: :challenges_users
  has_many :users_communities
  has_many :communities, through: :users_communities
  has_many :users_programs
  has_many :programs, through: :users_programs
  has_many :users_needs
  has_many :needs, through: :users_needs

  has_many :active_relationships, class_name: "Relationship",
                                  foreign_key: "follower_id",
                                  dependent: :destroy
  has_many :passive_relationships, class_name: "Relationship",
                                  foreign_key: "followed_id",
                                  dependent: :destroy

  has_many :relations

  has_one_attached :avatar
  has_one_attached :banner

  validates :avatar, content_type: ["image/png", "image/jpeg", "image/gif"]
  validates :banner, content_type: ["image/png", "image/jpeg", "image/gif"]

  after_create :create_feed
  # before_create :create_avatar
  after_create :send_confirmation_email, if: -> { !Rails.env.test? && ::User.devise_modules.include?(:confirmable) }
  before_create :create_coordinates
  accepts_nested_attributes_for :interests, allow_destroy: true
  enum status: [:active, :archived]

  after_update :mailchimp

  validates :nickname, uniqueness: true
  validates :email, uniqueness: true

  algoliasearch do
    attribute :nickname,
              :first_name,
              :last_name,
              :email,
              :skills,
              :age,
              :social_cat,
              :profession,
              :country,
              :city,
              :address,
              :interests
    geoloc :latitude, :longitude
  end

  def token_validation_response
    Api::UserSigninSerializer.new( self, root: false ).as_json
  end

  def geocode(object_type)
    object_id = self.id
    if :country?
      GeocoderWorker.perform_async(object_type, object_id)
    end
  end

  # We need to create feed for each object before creation in order to later have
  # posts in it
  def create_feed
    feed = ::Feed.create({user_id: self.id, object_type:'user'})
  end

  # Verifier of relation state.
  def follows?(object)
    @relation = Relation.find_by(user_id: self.id, resource_type: object.class.name, resource_id: object.id)
    if @relation.nil? or @relation.follows.nil?
      return false
    else
      return @relation.follows
    end
  end

  def clapped?(object)
    @relation = Relation.find_by(user_id: self.id, resource_type: object.class.name, resource_id: object.id)
    if @relation.nil? or @relation.has_clapped.nil?
      return false
    else
      return @relation.has_clapped
    end
  end

  def mailchimp
    if self.mail_newsletter == true
      MailchimpSubscriber.perform_async(self.id, subscribe=true)
    else
      MailchimpSubscriber.perform_async(self.id, subscribe=false)
    end
  end

  private

    # def send_devise_notification(notification, *args)
    #   devise_mailer.send(notification, self, *args).deliver_later
    # end

    def send_confirmation_email
      # ConfirmEmailWorker.perform_async(self.id)
      self.send_confirmation_instructions
    end

end
