class Comment < ApplicationRecord
  resourcify
  include RelationHelpers
  include Utils

  belongs_to :post
  belongs_to :user

  before_create :sanitize_content
  before_update :sanitize_content

  validates :content, presence: true
end
