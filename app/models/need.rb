class Need < ApplicationRecord
  # Each post must have a content must belong to user, must belong to a feed.
  # Document restrictions are applied in order to not let a user upload anything
  resourcify
  include RelationHelpers

  belongs_to :user
  belongs_to :project
  has_one :feed

  has_many :users_needs
  has_many :users, through: :users_needs
  has_many :needs_skills
  has_many :skills, through: :needs_skills
  # has_and_belongs_to_many :skills

  has_many_attached :documents
  # validates :documents, content_type: ["image/png",
  #                                      "image/jpeg",
  #                                      "image/gif",
  #                                      "text/csv",
  #                                      "application/pdf",
  #                                      "image/svg+xml",
  #                                      "application/x-tar",
  #                                      "image/tiff",
  #                                      "application/zip",
  #                                      "application/x-7z-compressed"
  #                                    ]
  # validates :documents, content_size: 31457280  # Document must be less than 30Mb

  enum status: [:active, :archived, :completed]

  validates :title, presence: true
  validates :content, presence: true

  after_create :create_feed

  def create_feed
    feed = Feed.create({need_id: self.id, object_type:'need'})
    save
  end
end
