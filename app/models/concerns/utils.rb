module Utils
  def geocode(object_type)
    object_id = self.id
    if self.country?
      GeocoderWorker.perform_async(object_type, object_id)
    end
  end

  def obj_type
    self.class.name
  end

  def sanitize_description
    self.description = ActionController::Base.helpers.sanitize(
      self.description,
      options = {
        tags: ['p', 'br', 'h1', 'h2', 'a', 'strong', 'em', 'u', 's', 'sub', 'sup', 'span', 'img', 'iframe', 'pre', 'blockquote'],
        attributes: ["height", "width", "style", "src", "href", "class", "allowfullscreen", "frameborder"]
      })
  end

  def sanitize_content
    self.content = ActionController::Base.helpers.sanitize(
      self.content,
      options = {
        tags: ['p', 'br', 'h1', 'h2', 'a', 'strong', 'em', 'u', 's', 'sub', 'sup', 'span', 'img', 'iframe', 'pre', 'blockquote'],
        attributes: ["height", "width", "style", "src", "href", "class", "allowfullscreen", "frameborder"]
      })
  end
end
