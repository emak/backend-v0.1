module RelationHelpers

  def claps_count
    Relation.where(resource_type: self.class.name, resource_id: self.id, has_clapped: true).count
  end

  def clappers
    Relation.where(resource_type: self.class.name, resource_id: self.id, has_clapped: true).select(:user_id)
  end

  def follower_count
    Relation.where(resource_type: self.class.name, resource_id: self.id, follows: true).count
  end

  def followers
    Relation.where(resource_type: self.class.name, resource_id: self.id, follows: true).select(:user_id)
  end

  def following_count
    if self.class.name == "User"
      Relation.where(user_id: self.id, follows: true).count
    end
  end

  def following
    if self.class.name == "User"
      Relation.where(user_id: self.id, follows: true).select(:resource_type, :resource_id)
    end
  end
end
