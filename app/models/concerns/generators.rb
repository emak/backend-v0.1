module Generators

    def create_avatars
      self.logo_url = "https://robohash.org/" + self.short_title + "?set=set4"
      self.banner_url = "https://robohash.org/" + self.short_title + "?set=set4"
    end

    def create_coordinates
      self.latitude = 38.074314 + (rand() * 2)
      self.longitude = -43.014136 + (rand() * 2)
    end

    def create_avatar
      self.logo_url = "https://robohash.org/" + self.email + "?set=set4"
    end

end
