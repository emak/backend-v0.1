class Community < ApplicationRecord
  resourcify
  include AlgoliaSearch
  include RelationHelpers
  include Generators
  include Utils


  has_and_belongs_to_many :community_tags
  has_and_belongs_to_many :skills
  has_and_belongs_to_many :interests
  has_one :feed
  has_many :users_communities
  has_many :users, through: :users_communities
  has_many :challenges_communities
  has_many :challenges, through: :users_communities
  has_one_attached :avatar
  has_one_attached :banner

  enum status: [:active, :archived]
  after_create :create_feed
  before_create :create_coordinates
  before_create :create_avatars

  before_create :sanitize_description
  before_update :sanitize_description

  validates :short_title, uniqueness: true
  validates :avatar, content_type: ["image/png", "image/jpeg", "image/gif"]
  validates :banner, content_type: ["image/png", "image/jpeg", "image/gif"]


  algoliasearch do
    attribute :id,
              :title,
              :short_title,
              :short_description,
              :interests,
              :skills
    geoloc :latitude, :longitude
  end

  # We need to create feed for each object before creation in order to later have
  # posts in it
  def create_feed
    feed = Feed.create({community_id: self.id, object_type:'community'})
  end
end
