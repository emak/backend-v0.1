class Project < ApplicationRecord
  # Posts must have a unique short name.
  resourcify
  include AlgoliaSearch
  include RelationHelpers
  include Generators
  include Utils

  has_many :users_projects
  has_many :users, through: :users_projects
  has_many :challenges_users
  has_many :challenges, through: :challenges_users
  has_many :needs_projects
  has_many :needs, through: :needs_projects
  has_and_belongs_to_many :skills
  has_and_belongs_to_many :interests
  has_one :feed

  has_one_attached :avatar
  has_one_attached :banner
  has_many_attached :documents

  validates :avatar, content_type: ["image/png", "image/jpeg", "image/gif"]
  # validates :avatar, content_size: 314573 # Less than 300 Kb per avatar image
  validates :banner, content_type: ["image/png", "image/jpeg", "image/gif"]
  # validates :avatar, content_size: 629145 # Less than 600 Kb per avatar image

  # TODO : FIX THE VALIDATION FOR HAS_MANY

  # validates :documents, content_type: ["image/png",
  #                                      "image/jpeg",
  #                                      "image/gif",
  #                                      "text/csv",
  #                                      "application/pdf",
  #                                      "image/svg+xml",
  #                                      "application/x-tar",
  #                                      "image/tiff",
  #                                      "application/zip",
  #                                      "application/x-7z-compressed"
  #                                    ]
  # validates :documents, content_size: 31457280  # Document must be less than 30Mb


  validates :short_title, uniqueness: true

  after_create :create_feed

  before_create :create_coordinates
  before_create :create_avatars

  before_create :sanitize_description
  before_update :sanitize_description

  enum status: [:active, :archived, :draft, :completed]

  # After the model is validated geocode !
  # after_validation :geocode, on: [ :update ]

  algoliasearch do
    attribute :id,
              :title,
              :short_title,
              :short_description,
              :skills,
              :interests,
              :status
    geoloc :latitude, :longitude
  end

  # We need to create feed for each object before creation in order to later have
  # posts in it
  def create_feed
    feed = Feed.create({project_id: self.id, object_type:'project'})
    save
  end

end
