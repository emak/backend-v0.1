class GeocoderWorker
  include Sidekiq::Worker

  def perform(object_type, object_id)
    @Object = object_type.singularize.classify.constantize.find(object_id)
    # if object_type == "user"
    #   @Object = User.find(object_id)
    # elsif object_type == "project"
    #   @Object = Project.find(object_id)
    # elsif object_type == "community"
    #   @Object = Community.find(object_id)
    # end

    address = [@Object.address, @Object.city, @Object.country].compact.join(', ')
    # First try the full Address
    results = Geocoder.search(address)
    # If there is no results try the city only
    if results.empty?
      address = [@Object.city, @Object.country].compact.join(', ')
    end
    # If there is a result update, else pass
    if !results.empty?
      @Object.latitude = results.first.coordinates[0]
      @Object.longitude = results.first.coordinates[1]
      @Object.save!
    end
  end
end
