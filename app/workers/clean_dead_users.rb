class CleanDeadUsers
  include Sidekiq::Worker

  def perform
    User.all.map do |user|
      unless user.confirmed
        dt = (Time.now - user.created_at) / 1.hours
        if dt > 72
          user.destroy
        end
      end
    end
  end
end
