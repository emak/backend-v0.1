class UserJoinedEmailWorker
  include Sidekiq::Worker

  def perform(object_type, object_id, joiner_id)
    @joiner = User.find(joiner_id)
    @object = object_type.constantize.find(object_id)
    @object.users.map do |member|
      if member.has_role? :owner, @object
        UserJoinedMailer.user_joined(member, @object, @joiner).deliver
      end
    end
  end
end
