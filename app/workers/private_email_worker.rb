class PrivateEmailWorker
  include Sidekiq::Worker
	#current_user.id, params[:sender_id], params[:object], params[:content]
  def perform(from_id, to_id, object, content)
    @from = User.find(from_id)
    @to = User.find(to_id)
    PrivateMailer.send_private_email(@from, @to, object, content).deliver
  end
end
