class NotificationEmailWorker
  include Sidekiq::Worker

  def perform(user_id, message, url, title)
    @user = User.find(user_id)
    NotificationMailer.notify(@user, message, url, title).deliver
  end
end
