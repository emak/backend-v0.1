class Api::PagesController < ApplicationController
  def index
    render json: { content: 'Welcome to the JOGL API' }
  end
end
