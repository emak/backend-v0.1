class Api::FeedsController < ApplicationController
  before_action :find_feed, only: [:show]
  before_action :authenticate_user!, only: [:index]
  before_action :make_user_feed, only: [:index]

  # before_action :sanitize, only: [:create, :update]

  def index
    @pagy, @myfeed = pagy_array(@posts)
    render json: @myfeed, each_serializer: Api::PostSerializer, status: :ok
  end

  def show
    @pagy, @somefeed = pagy_array(@feed.posts.order('created_at DESC'))
    render json: @somefeed, each_serializer: Api::PostSerializer, status: :ok
  end


  private
    def find_feed
      @feed = Feed.find(params[:id])
      if @feed.nil?
        render json: {data: "Feed not found"}, status: :not_found
      end
    end

    def make_user_feed
      @posts = []
      @posts += current_user.feed.posts
      current_user.following.map do |object|
        @object = object.resource_type.constantize.find(object.resource_id)
        @posts += @object.feed.posts
      end
      @posts = @posts.sort_by{|e| e[:created_at]}.reverse.uniq
    end
end
