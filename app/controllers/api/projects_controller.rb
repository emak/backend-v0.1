class Api::ProjectsController < ApplicationController
  include Api::Follow
  include Api::Members
  include Api::Upload
  include Api::Relations
  include Api::Utils

  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :find_project, except: [:index, :create, :my_projects, :short_title_exist]
  before_action :authenticate_user!, only: [:create, :update, :destroy, :invite, :join,
                                            :upload, :update_member, :members_list, :my_projects]
  before_action :set_obj, except: [:index, :create, :show, :destroy, :short_title_exist]
  before_action :is_admin, only: [:update, :invite, :upload, :update_member, :remove_member]
  # Before action for the relations Api::Relations
  before_action :find_relation, only: [:unfollow, :unclap, :follow, :clap]
  before_action :relation_exists?, only: [:unfollow, :unclap]
  before_action :relation_on_empty, only: [:clap, :follow]

  def index
    @pagy, @projects = pagy(Project.where.not(status: 1).all)
    json_response(@projects)
  end

  def index_needs
    @pagy, @needs = pagy(@project.needs)
    render json: @needs, each_serializer: Api::NeedSerializer
  end

  def my_projects
    @projects = Project.with_role(:owner, current_user)
    @projects += Project.with_role(:admin, current_user)
    @projects += Project.with_role(:member, current_user)
    @pagy, @projects = pagy_array(@projects.uniq)
    render json: @projects
  end

  def create
    if !Project.where(short_title: params[:project][:short_title]).empty?
      render json: {data: "ShortTitle is already taken"}, status: :unprocessable_entity
    else
      @project = Project.new(project_params)
      @project.status = 2
      @project.creator_id = current_user.id
      @project.users << current_user
      save_interests params[:project][:interests], @project
      save_skills params[:project][:skills], @project
      if @project.save
        current_user.add_role :owner, @project
        current_user.add_role :admin, @project
        current_user.add_role :member, @project
        @project.geocode('project')
        render json: {id: @project.id, data: "Success"}, status: :created
      end
    end
  end

  def show
    json_response(@project)
  end

  def update
    save_interests params[:project][:interests], @project
    save_skills params[:project][:skills], @project
    if params[:project][:creator_id].present?
      unless current_user.has_role? :owner, @project
        render json: {data: "You cannot change the creator_id"}, status: :forbidden
      else
        @project.roles.find_by(resource_type: @project.class.name, resource_id: @project.id, name: "owner").delete
        @new_owner = User.find(params[:project][:creator_id])
        if @new_owner.nil?
          render json: {data: "New owner does not exist"}, status: :not_found
        else
          @new_owner.add_role :owner, @project
        end
      end
    end
    if @project.update_attributes(project_params)
      @project.geocode('project')
      if @project.save!
        render json: @project, status: :ok
      end
    else
      render json: {data: "Something went wrong :("}, status: :unprocessable_entity
    end
  end

  def destroy
    # why count the users in a project?
    # this would be more logical @project.users.delete_all followed by status modification
    # Leo: Because if the project has only one user we consider it a "test" aka you can ACTUALLY deleted it
    # Else, it stays as archived
    if @project.users.count == 1
      @project.destroy
      render json: {data: "Project destroyed"}, status: :ok
    else
      @project.update_attributes({status: "archived"})
      render json: {data: "Project archived"}, status: :ok
    end
  end
  #
  # def upload_document
  #   documents = params[:documents]
  #   if !documents.nil?
  #     documents.each do |document|
  #       @project.documents.attach(document)
  #     end
  #     if !@project.documents.attached?
  #       render json: {data: "Something went wrong"}, status: :unprocessable_entity and return
  #     end
  #     render json: {data: "documents uploaded"}, status: :ok
  #   end
  # end

  private
    def find_project
      @project = Project.find(params[:id])
      if @project.nil?
        render json: {data: "Project not found"}, status: :not_found
      end
    end

    def set_obj
      @obj = @project
    end

    def project_params
      params.require(:project).permit(:title, :short_title, :banner_url, :logo_url, :description, :short_description,
                                      :country, :city, :address, :is_private, :creator_id, :stranger_id, :current_user_id)
    end
end
