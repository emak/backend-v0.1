class Api::CommunitiesController < ApplicationController
  include Api::Follow
  include Api::Members
  include Api::Upload
  include Api::Relations
  include Api::Utils

  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :find_community, except: [:create, :index, :my_communities, :short_title_exist]
  before_action :authenticate_user!, except: [:index, :show, :followers]
  before_action :set_obj, except: [:index, :create, :show, :short_title_exist]
  before_action :is_admin, only: [:update, :invite, :upload, :update_member, :remove_member]

  # Before action for the relations Api::Relations
  before_action :find_relation, only: [:unfollow, :unclap, :follow, :clap]
  before_action :relation_exists?, only: [:unfollow, :unclap]
  before_action :relation_on_empty, only: [:clap, :follow]

  def index
    @pagy, @communities = pagy(Community.where.not(status: 'archived').all)
    json_response(@communities)
  end

  def my_communities
    @communities = Community.with_role(:owner, current_user)
    @communities += Community.with_role(:admin, current_user)
    @communities += Community.with_role(:member, current_user)
    @pagy, @communities = pagy_array(@communities.uniq)
    render json: @communities
  end

  # On creation the creator get all roles, skills and interests are added manually
  def create
    if !Community.where(short_title: params[:community][:short_title]).empty?
      render json: {data: "ShortTitle is already taken"}, status: :unprocessable_entity and return
    end
    @community = Community.new(community_params)
    @community.creator_id = current_user.id
    @community.status = 'active'
    @community.users << current_user
    save_interests params[:community][:interests], @community
    save_skills params[:community][:skills], @community
    if @community.save
      @community.geocode('community')
      current_user.add_role :owner, @community
      current_user.add_role :admin, @community
      current_user.add_role :member, @community
      render json: {id: @community.id, data: "Success"}, status: :created
    end
  end

  def show
    json_response(@community)
  end

  # on update the interests and skills of the object are rewritten.
  def update
    save_interests params[:community][:interests], @community
    save_skills params[:community][:skills], @community
    if !params[:community][:creator_id].nil?
      if @community.creator_id != params[:community][:creator_id] and @community.creator_id != current_user.id
        render json: {data: "You cannot change the creator_id"}, status: :forbidden
      end
    end
    if @community.update_attributes(community_params)
      @community.geocode('community')
      if @community.save!
        json_response(@community)
      end
    else
      render json: {data: "Something went wrong !"}, status: :unprocessable_entity
    end
  end

  def destroy
    if @community.users.count == 1
      @community.destroy
      render json: {data: "Community destroyed"}, status: :ok
    else
      @community.update_attributes({status: "archived"})
      render json: {data: "Community archived"}, status: :ok
    end
  end

  private
    def find_community
      @community = Community.find(params[:id])
      if @community.nil?
        render json: {data: "Community not found"}, status: :not_found
      end
    end

    def set_obj
      @obj = @community
    end

    def  community_params
      params.require(:community).permit(:title, :short_title, :description, :short_description,
                                        :country, :city, :address, :is_private, :creator_id, :stranger_id, :current_user_id)
    end
end
