class Api::PostsController < ApplicationController
  include Api::Relations
  include Api::Upload

  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :find_post, only: [:show, :update, :destroy, :comment, :upload_document, :unfollow, :unclap, :follow, :clap]

  before_action :authenticate_user!, only: [:create, :update, :destroy, :comment, :upload, :unfollow, :unclap, :follow, :clap]
  before_action :set_obj, except: [:index, :create, :show, :destroy, :comment]

  before_action :find_relation, only: [:unfollow, :unclap, :follow, :clap]
  before_action :relation_exists?, only: [:unfollow, :unclap]
  before_action :relation_on_empty, only: [:clap, :follow]

  before_action :is_allowed, only: [:update, :destroy]

  # before_action :sanitize, only: [:create, :update]

  def show
    json_response(@post)
  end

  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id
    @feed = Feed.find(params[:post][:feed_id])
    @from = @feed.object_type.camelize.constantize.find(@feed[@feed.object_type+'_id'])
    @post.from_object = @from.class.name
    @post.from_id = @from.id
    if @from.class.name == 'User'
      @post.from_name = @from.nickname
    else
      @post.from_name = @from.title
    end
    if @post.save and !@feed.nil? and !@from.nil?
      @feed.posts << @post
      # First let's render the JSON because the post went through
      render json: {id: @post.id, data: "Your post has been published"}, status: :created
      # Then we handle the different mentions "offline"
      save_mentions(params[:post][:mentions])
    else
      render json: {data: "Something went wrong"}, status: :unprocessable_entity
    end
  end

  def update
    if @post.update_attributes(post_params)
      render json: {data: "Post has been updated"}, status: :ok
    end
    # Now we handle the mentions
    save_mentions(params[:post][:mentions])
  end

  def destroy
    if @post.destroy
      render json: {data: "Your post has been deleted"}, status: :ok
    end
  end

  def comment
    case request.method_symbol
    when :post
      @comment = Comment.new(user_id: current_user.id,
                             post_id: @post.id,
                             content: params[:comment][:content])
      if @comment.save
        @post.comments << @comment
        # First let's render the JSON because the post went through
        render json: {data: "Your comment has been published"}, status: :created
        save_mentions(params[:comment][:mentions])
      end
    when :patch
      @comment = Comment.find(params[:comment_id])
      is_allowed @comment
      if @comment.nil?
        render json: {data: "Cannot find comment id"}
      end
      @comment.update_attributes(content: params[:comment][:content])
      render json: {data: "Your post has been updated"}, status: :ok
      save_mentions(params[:comment][:mentions])
    when :delete
      @comment = Comment.find(params[:comment_id])
      is_allowed @comment
      @comment.destroy
      render json: {data: "Comment deleted"}, status: :ok
    end
  end

  private

    def save_mentions(mentions)
      if !mentions.nil?
        mentions.map do |mention|
          # Create a mention in the table
          logger.debug(mention)
          @mention = Mention.new(post_id: @post.id,
                                 obj_type: mention[:obj_type],
                                 obj_id: mention[:obj_id],
                                 obj_match: mention[:obj_match])
          # We check that the mention is valid, this means that it has a unique set of post and object mentions
          # Basically we do not store double mentions as double but just once
          if @mention.valid?
            # The we do the whole logic for the mentions
            @object = get_object_mention(mention)
            if !@object.nil?
              @mention.save
              @post.mentions << @mention
              unless @object.feed.posts.exists?(@post.id)
                @object.feed.posts << @post
              end
            end
            if @object.class.name == 'User'
              NotificationEmailWorker.perform_async(@object.id, "Someone mentionned you in a post!", @post.from_object.downcase + '/' + @post.from_id.to_s, "Someone mentionned you!")
            end
          end
        end
      end
    end

    def find_post
      @post = Post.find(params[:id])
      if @post.nil?
        render json: {data: "Post not found"}, status: :not_found
      end
    end

    def set_obj
      @obj = @post
    end

    def get_object_mention(mention)
      @object = mention[:obj_type].humanize.constantize.find(mention[:obj_id])
      return @object
    end

    def post_params
      params.require(:post).permit(:user_id, :content, :media, :feed_id, :is_need)
    end

    def is_allowed(object=@post)
      unless object.user_id == current_user.id
        feed = Feed.find(@post.feed_id)
        parent = feed.object_type.camelize.constantize.find(feed[feed.object_type+"_id"])
        unless current_user.has_role? :admin, parent
          render json: {data: "Forbidden"}, status: :forbidden and return
        end
      end
    end

end
