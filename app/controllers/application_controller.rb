class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  include ActionController::MimeResponds
  include Response
  include ExceptionHandler
  include Pagy::Backend
  respond_to :json

  before_action :configure_permitted_parameters, if: :devise_controller?
  after_action { pagy_headers_merge(@pagy) if @pagy }
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in)
  end

end
