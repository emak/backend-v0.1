module Api::Utils
  def is_admin
    unless current_user.has_role? :admin, @obj
      render json: {data: "Forbidden"}, status: :forbidden
    end
  end

  def save_skills(skills, obj)
    unless skills.nil?
      obj.skills.delete_all
      skills.each do |skill|
        @skill = Skill.find_by(skill_name: skill)
        if @skill.nil?
           @skill = Skill.new(skill_name: skill)
        end
        obj.skills << @skill
      end
    end
  end

  def save_interests(interests, obj)
    unless interests.nil?
      obj.interests.delete_all
      interests.each do |interest|
        obj.interests << Interest.find(interest)
      end
    end
  end

  def nickname_exist
    if User.where(nickname: params[:nickname]).count > 0
      render json: {data: "Nickname already exists"}, status: :forbidden
    else
      render json: {data: "Nickname is available"}, status: :ok
    end
  end

  def short_title_exist
    klass = controller_name.classify.constantize
    # klass = params[:object_type].capitalize.singularize.constantize
    if klass.where(short_title: params[:short_title]).count > 0
      render json: {data: "short_title already exists"}, status: :forbidden
    else
      render json: {data: "short_title is available"}, status: :ok
    end
  end

end
