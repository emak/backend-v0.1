module Api::Members

  # With a given user id returns find or not found if the given user is a member
  # of the given object

  def is_member
    @user = User.find(params[:user_id])
    if @obj.users.include?(@user)
      render json: {data: "The current user is object member"}, status: :found
    else
      render json: {data: "The current user is not a object member"}, status: :not_found
    end
  end

  # Same logic is applied here, the given user is deleted from the relation of
  # the given object
  def remove_member
    @user = User.find(params[:user_id])
    if @obj.users.include?(@user)
      unless @user.has_role? :owner, @obj
        @obj.users.delete(@user)
        if @user.has_role? :member, @obj
          @user.remove_role :member, @obj
        end
        if @user.has_role? :admin, @obj
          @user.remove_role :admin, @obj
        end
        render json: {data: "The user was removed"}, status: :found
      else
        render json: {data: "Forbidden"}, status: :forbidden
      end
    else
      render json: {data: "The user is not in the object"}, status: :not_found
    end
  end

  # Same logic is applied here, with given object and given user we verify user role
  # for that object, if he has the rights on the given object he can update his role
  def update_member
    @user = User.find(params[:user_id])
    if @obj.users.include?(@user) and !params[:previous_role].nil? and !params[:new_role].nil?
      if params[:new_role] == "owner" and !current_user.has_role? :owner, @obj
        render json: {data: "Only an owner can set an owner"}, status: :forbidden and return
      end
      if params[:previous_role] == "owner" and !current_user.has_role? :owner, @obj
        if User.with_role(:owner, @obj).count < 2
          render json: {data: "You cannot have zero owner"}, status: :forbidden and return
        end
      end
      for role in get_roles_list(params[:previous_role]) do
        @user.remove_role role, @obj
      end
      for role in get_roles_list(params[:new_role]) do
        @user.add_role role, @obj
      end
      render json: {data: "Role set"}, status: :ok
    else
      render json: {data: "Malformed request"}, status: :unprocessable_entity
    end
  end

  # Returns all available roles for a given role
  def get_roles_list(role)
    if role == 'owner'
      return ['owner', 'admin', 'member']
    elsif role == 'admin'
      return ['admin', 'member']
    elsif role == 'member'
      return ['member']
    elsif role == 'pending'
      return ['pending']
    else
      return []
    end
  end

  def members_list
    render json: @obj, serializer: Api::MembersSerializer
  end

  # We verify if the object is not archived if not we add the user/object relation
  # for Project and Community objects we send a confirmation email
  def join
    if @obj.status == 'archived'
      render json: {data: "The object is archived and cannot be joined"}, status: :forbidden and return
    end
    unless @obj.users.exists?(current_user.id)
      @obj.users << current_user
      if @obj.class.name == "Challenge" or @obj.class.name == "Need"
        current_user.add_role :member, @obj
      else
        if @obj.is_private
          current_user.add_role :pending, @obj
        else
          current_user.add_role :member, @obj
        end
      end
      UserJoinedEmailWorker.perform_async(@obj.class.name, @obj.id, current_user.id)
      render json: {data: "User has joined the object"}, status: :ok
    else
      render json: {data: "User has already joined the object"}, status: :forbidden
    end
  end

  # for a given object the user relation is deleted
  def leave
    if @obj.users.exists?(current_user.id)
      @obj.users.delete(current_user)
      current_user.remove_role :pending, @obj
      current_user.remove_role :member, @obj
      current_user.remove_role :admin, @obj
      current_user.remove_role :owner, @obj
      if @obj.save
        render json: {data: "User has left the object"}, status: :ok
      end
    else
      render json: {data: "User is not a member of the object"}, status: :forbidden
    end
  end

  # Depending on a given param the user is either looked in the database or an email
  # is sent to the email given in the param
  # if the user is found in the database his role is changed and another type of email is sent
  def invite
    if params[:stranger_email].present?
      InviteStrangerEmailWorker.perform_async(current_user.id, @obj.class.name, @obj.id, params[:stranger_email])
    else
      params[:user_ids].map do |user_id|
        @joiner = User.where(id: user_id).first
        unless @joiner.nil?
          unless @obj.users.include?(@joiner)
            @obj.users << @joiner
            @joiner.add_role :member, @obj
            InviteUserEmailWorker.perform_async(current_user.id, @obj.class.name, @obj.id, @joiner.id)
          end
        end
      end
      render json: {data: "Users invited"}, status: :ok
    end
  end
end
