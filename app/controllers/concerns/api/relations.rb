module Api::Relations
  extend ActiveSupport::Concern
  #
  # included do
  #   before_action :find_relation, only: [:unfollow, :unclap, :follow, :clap]
  #   before_action :relation_exists?, only: [:unfollow, :unclap]
  #   before_action :relation_on_empty, only: [:clap, :follow]
  # end

  def follow
    @relation.follows = true
    save_relation("Followed")
  end

  def unfollow
    @relation.follows = false
    save_relation("Unfollowed")
  end

  def clap
    @relation.has_clapped = true
    save_relation("Claped")
  end

  def unclap
    @relation.has_clapped = false
    save_relation("Unclaped")
  end

  private
    def save_relation(action)
      if @relation.save
        render json: {data: "#{action} successfully"}, status: :ok
      else
        render json: {data: "Something went wrong"}, status: :unprocessable_entity
      end
    end

    def relation_on_empty
     if @relation.nil?
       @relation = Relation.new(user_id: current_user.id, resource_type: @obj.class.name, resource_id: @obj.id)
     end
    end

    def find_relation
      logger.debug(current_user.id)
      logger.debug(@obj.id)
      @relation = Relation.find_by(user_id: current_user.id, resource_type: @obj.class.name, resource_id: @obj.id)
    end

    def relation_exists?
      if @relation.nil?
        render json: {data: "Relation does not exist"}, status: :not_found
      end
    end

    def relation_params
      params.permit(:resource_type, :resource_id)
    end

end
