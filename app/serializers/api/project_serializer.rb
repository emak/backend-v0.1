class Api::ProjectSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :id,
             :title,
             :short_title,
             :logo_url,
             :banner_url,
             :description,
             :short_description,
             :creator,
             :status,
             :skills,
             :interests,
             :documents,
             :documents_feed,
             :geoloc,
             :country,
             :city,
             :address,
             :feed_id,
             :is_private,
             :users,
             :is_owner,
             :is_admin,
             :is_member,
             :has_clapped,
             :has_followed,
             :claps_count,
             :follower_count,
             :members_count,
             :needs_count





end
