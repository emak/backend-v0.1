class Api::ChallengeSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :id,
             :title,
             :banner_url,
             :description,
             :short_description,
             :rules,
             :faq,
             :status,
             :skills,
             :interests,
             :documents,
             :geoloc,
             :country,
             :city,
             :address,
             :feed_id,
             :users,
             :program,
             :launch_date,
             :end_date,
             :final_date,
             :is_owner,
             :is_admin,
             :is_member,
             :has_clapped,
             :has_followed,
             :claps_count,
             :follower_count,
             :members_count,
             :projects_count

  def program
    @program = object.program
    unless @program.nil?
      {
        id: @program.id,
        title: @program.title,
        short_title: @program.short_title
      }
    else
        {
          id: -1,
          title: object.title
        }
    end
  end

  def projects_count
    object.projects.count
  end

end
