class Api::ProgramChallengeSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :challenges

# Only needed params for challenges of a challenge are returned
  def challenges
    object.challenges.map do |challenge|
      {
        id: challenge.id,
        title: challenge.title,
        banner_url: banner_url(challenge),
        status: challenge.status,
        skills: skills(challenge),
        interests: interests(challenge),
        geoloc: geoloc(challenge),
        is_owner: is_owner(challenge),
        is_admin: is_admin(challenge),
        is_member: is_member(challenge),
        users: users(challenge),
        has_clapped: has_clapped(challenge),
        has_followed: has_followed(challenge),
        claps_count: challenge.claps_count,
        follower_count: challenge.follower_count,
        needs_count: needs_count(challenge)
      }
    end
  end
end
