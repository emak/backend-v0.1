class Api::UserSigninSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Utilsserializerhelper

  attributes  :id,
              :email,
              :first_name,
              :last_name,
              :nickname,
              :age,
              :social_cat,
              :profession,
              :category,
              :affiliation,
              :country,
              :city,
              :address,
              :phone_number,
              :bio,
              :status,
              :mail_newsletter,
              :mail_weekly,
              :sign_in_count,
              :confirmed_at,
              :interests,
              :skills,
              :feed_id,
              :geoloc,
              :logo_url,
              :claps_count,
              :follower_count,
              :following_count

end
