class Api::ProjectIndexSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :id,
             :title,
             :banner_url,
             :short_description,
             :status,
             :skills,
             :interests,
             :geoloc,
             :is_private,
             :is_owner,
             :is_admin,
             :is_member,
             :users,
             :has_clapped,
             :has_followed,
             :claps_count,
             :follower_count,
             :needs_count



end
