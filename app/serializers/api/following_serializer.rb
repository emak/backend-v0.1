class Api::FollowingSerializer < ActiveModel::Serializer
  has_many :projects, serializer: Api::ProjectSerializer
  has_many :communities, serializer: Api::CommunitySerializer
  has_many :users, serializer: Api::UserSerializer
  has_many :challenges, serializer: Api::ChallengeSerializer


  def projects
    object.following.where(resource_type: "Project").map do |project|
      @obj = Project.find(project.resource_id)
    end
  end

  def communities
    object.following.where(resource_type: "Community").map do |community|
      @obj = Community.find(community.resource_id)
    end
  end

  def users
    object.following.where(resource_type: "User").map do |user|
      @obj = User.find(user.resource_id)
    end
  end

  def challenges
    object.following.where(resource_type: "Challenge").map do |challenge|
      @obj = Challenge.find(challenge.resource_id)
    end
  end

end
