class Api::ProgramSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :id,
             :title,
             :short_title,
             :banner_url,
             :description,
             :status,
             :users,
             :feed_id,
             :short_description,
             :faq,
             :enablers,
             :ressources,
             :launch_date,
             :end_date,
             :is_owner,
             :is_admin,
             :is_member,
             :has_clapped,
             :has_followed,
             :claps_count,
             :follower_count,
             :members_count

end
