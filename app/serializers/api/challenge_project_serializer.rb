class Api::ChallengeProjectSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :projects

# Only needed params for projects of a challenge are returned
  def projects
    object.projects.map do |project|
      @relation = ChallengesProject.find_by(challenge_id: object.id, project_id: project.id)
      {
        id: project.id,
        title: project.title,
        banner_url: banner_url(project),
        short_description: project.short_description,
        status: project.status,
        creator: creator(project),
        challenge_id: object.id,
        challenge_status: @relation.status,
        skills: skills(project),
        interests: interests(project),
        geoloc: geoloc(project),
        is_private: project.is_private,
        is_owner: is_owner(project),
        is_admin: is_admin(project),
        is_member: is_member(project),
        users: users(project),
        has_clapped: has_clapped(project),
        has_followed: has_followed(project),
        claps_count: project.claps_count,
        follower_count: project.follower_count,
        needs_count: needs_count(project)
      }
    end
  end
end
