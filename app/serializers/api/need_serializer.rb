class Api::NeedSerializer < ActiveModel::Serializer
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper



  attributes :id,
             :title,
             :content,
             :creator,
             :documents,
             :status,
             :feed_id,
             :users,
             :end_date,
             :skills,
             :is_owner,
             :is_member,
             :has_followed,
             :follower_count,
             :claps_count,
             :has_clapped

end
