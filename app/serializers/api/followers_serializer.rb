class Api::FollowersSerializer < ActiveModel::Serializer
	include Api::Relationsserializerhelper
  attributes :followers

  def followers
    object.followers.map do |user_id|
      user = User.find(user_id.user_id)
      if user.avatar.attached?
        logo_url= Rails.application.routes.url_helpers.rails_blob_url(user.avatar)
      else
        logo_url= user.logo_url
      end
      {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
				logo_url: logo_url,
				has_followed: has_followed(user)
      }
    end
  end

end
