class Api::MentionSerializer < ActiveModel::Serializer
  attributes :obj_type,
             :obj_id,
             :obj_match

end
