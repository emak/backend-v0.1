class Api::PostSerializer < ActiveModel::Serializer
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper
  include Api::Relationsserializerhelper

  attributes :id,
             :content,
             :media,
             :creator,
             :mentions,
             :from,
             :comments,
             :created_at,
             :documents,
             :claps_count,
             :has_clapped

  def from
    unless object.from_object.nil?
      {
        object_type: object.from_object.downcase,
        object_id: object.from_id,
        object_name: object.from_name
      }
    else
      {
        object_type: "impossible",
        object_id: 0,
        object_name: "Bollocks"
      }
    end
  end

  def comments
    object.comments.map do |comment|
      Api::CommentSerializer.new(comment, scope: scope, root: false, event: object)
    end
  end

  def mentions
    object.mentions.map do |mention|
      Api::MentionSerializer.new(mention, scope: scope, root: false, event: object)
    end
  end
end
