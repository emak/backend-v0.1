class Api::UserSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Utilsserializerhelper

  attributes  :id,
              :email,
              :first_name,
              :last_name,
              :nickname,
              :age,
              :social_cat,
              :profession,
              :affiliation,
              :category,
              :country,
              :city,
              :address,
              :phone_number,
              :bio,
              :short_bio,
              :status,
              :mail_newsletter,
              :mail_weekly,
              :sign_in_count,
              :confirmed_at,
              :interests,
              :skills,
              :feed_id,
              :geoloc,
              :logo_url,
              :claps_count,
              :is_admin,
              :has_clapped,
              :has_followed,
              :follower_count,
              :following_count

end
