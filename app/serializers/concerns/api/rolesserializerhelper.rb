module Api::Rolesserializerhelper
# Verify user role for a given object
  def is_owner(obj=nil)
    if obj.nil?
      obj = object
    end
    unless current_user.nil?
      current_user.has_role?(:owner, obj)
    else
      false
    end
  end

  def is_admin(obj=nil)
    if obj.nil?
      obj = object
    end
    unless current_user.nil?
      current_user.has_role?(:admin, obj)
    else
      false
    end
  end

  def is_member(obj=nil)
    if obj.nil?
      obj = object
    end
    unless current_user.nil?
      current_user.has_role?(:member, obj)
    else
      false
    end
  end

end
