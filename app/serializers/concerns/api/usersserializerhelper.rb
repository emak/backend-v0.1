module Api::Usersserializerhelper

  include Rails.application.routes.url_helpers

  def users(obj = nil)
    if obj.nil?
      obj = object
    end
    obj.users.map do |user|
      logo_url = get_logo_url(user)
      {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        owner: user.has_role?(:owner, object),
        admin: user.has_role?(:admin, object),
        member: user.has_role?(:member, object),
        logo_url: logo_url
      }
    end
  end

  def members_count(obj=nil)
    if obj.nil?
      obj = object
    end
    User.with_role(:member, obj).count
  end

  def creator(obj=nil)
    if obj.nil?
      obj = object
    end
    if obj.class.name == "Post" or obj.class.name == "Comment" or obj.class.name == "Need"
      user = User.find(obj.user_id)
      logo_url = get_logo_url(user, variant_size="40x40^")
    else
      user = User.find(obj.creator_id)
      logo_url = get_logo_url(user)
    end
    {
      id: user.id,
      email: user.email,
      first_name: user.first_name,
      last_name: user.last_name,
      logo_url: logo_url
    }
  end

  def get_logo_url(user, variant_size="200x200^")
    if user.avatar.attached?
      variant = user.avatar.variant(resize: variant_size)
      logo_url = Rails.application.routes.url_helpers.rails_representation_url(variant)
      # logo_url= Rails.application.routes.url_helpers.rails_blob_url(user.avatar)
    else
      logo_url = user.logo_url
    end
  end

end
