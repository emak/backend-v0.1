module Api::Utilsserializerhelper

  # Return a good format for logo of the object
  def logo_url(obj=nil)
    if obj.nil?
      obj = object
    end
    if !obj.avatar.attachment.nil?
      variant = obj.avatar.variant(resize: "200x200^")
      Rails.application.routes.url_helpers.rails_representation_url(variant)
      # Rails.application.routes.url_helpers.rails_blob_url(obj.avatar)
    else
      object.logo_url
    end
  end
  # Return a good format for banner of the object

  def banner_url(obj=nil)
    if obj.nil?
      obj = object
    end
    if !obj.banner.attachment.nil?
      variant = obj.banner.variant(resize: "400x400^")
      Rails.application.routes.url_helpers.rails_representation_url(variant)
      # Rails.application.routes.url_helpers.rails_blob_url(obj.banner)
    else
      ""
    end
  end

  def documents
    if object.documents.attached?
      object.documents.attachments.map do |document|
        {
          id: document.blob.id,
          content_type: document.blob.content_type,
          filename: document.blob.filename,
          url: Rails.application.routes.url_helpers.rails_blob_url(document)
        }
      end
    else
      []
    end
	end

	def documents_feed
    unless object.feed.nil?
			attachments = get_feed_attachments(object)
			attachments.flatten
		end
  end


	def get_feed_attachments(obj)
		obj.feed.posts.map do |post|
			if post.documents.attached?
				post.documents.attachments.map do |document|
					{
						id: document.blob.id,
						content_type: document.blob.content_type,
						filename: document.blob.filename,
						url: Rails.application.routes.url_helpers.rails_blob_url(document)
					}
				end
			end
		end
	end


  def geoloc(obj = nil)
    if obj.nil?
      obj = object
    end
    {
      lat: obj.latitude,
      lng: obj.longitude
    }
  end

  def interests(obj=nil)
    if obj.nil?
      obj = object
    end
    obj.interests.map do |interest|
      interest.id
    end
  end

  def skills(obj=nil)
    if obj.nil?
      obj = object
    end
    obj.skills.map do |skill|
      skill.skill_name
    end
  end

  def feed_id
    object.feed.id
  end

  def needs_count(obj=nil)
    if obj.nil?
      obj = object
    end
    obj.feed.posts.where(is_need: true).count
    # 6
  end

end
