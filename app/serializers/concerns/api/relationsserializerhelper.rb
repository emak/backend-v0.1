module Api::Relationsserializerhelper
  # Helpers for clap and follow
  def has_clapped(obj=nil)
    if obj.nil?
      obj = object
    end
    unless current_user.nil?
      current_user.clapped? obj
    else
      false
    end
  end

  def has_followed(obj=nil)
    if obj.nil?
      obj = object
    end
    unless current_user.nil?
      current_user.follows? obj
    else
      false
    end
  end

end
