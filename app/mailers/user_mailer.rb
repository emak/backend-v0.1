class UserMailer < ApplicationMailer

  def registration_confirmation(user, redirect_url)
    @user = user
    @redirect_url = redirect_url
    mail(:to => "<#{user.email}>", :subject => "JOGL - Registration Confirmation")
  end
end
