require 'digest/sha2'
class ApplicationMailer < ActionMailer::Base
  default "message-id" => "<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@jogl.io>"
  default from: 'JOGL <hello@jogl.io>'
  layout 'mailer'
end
