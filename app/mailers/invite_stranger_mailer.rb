class InviteStrangerMailer < ApplicationMailer

  def invite_stranger(owner, object, stranger)
    @owner = owner
    @object = object
    @stranger = stranger
    mail(:to => "<#{stranger}>", :subject => "JOGL - You have been invited to join a " + @object.class.name.downcase)
  end
end
