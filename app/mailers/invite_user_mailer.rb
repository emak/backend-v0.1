class InviteUserMailer < ApplicationMailer

  def invite_user(owner, object, joiner)
    @owner = owner
    @object = object
    @joiner = joiner
    mail(:to => "<#{joiner.email}>", :subject => "JOGL - You have been invited to join a " + @object.class.name.downcase)
  end
end
