class UserJoinedMailer < ApplicationMailer

  def user_joined(owner, object, joiner)
    @owner = owner
    @object = object
    @joiner = joiner
    mail(:to => "<#{owner.email}>", :subject => "JOGL - A user joined your " + @object.class.name.downcase)
  end
end
