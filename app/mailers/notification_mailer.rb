class NotificationMailer < ApplicationMailer

  def notify(user, message, url, title)
    @user = user
    @message = message
    @url = url
    @title = title
    mail(:to => "<#{user.email}>", :subject => "<#{title}>")
  end
end
