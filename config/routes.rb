Rails.application.routes.draw do


  mount_devise_token_auth_for 'User', at: 'api/users'

  devise_for :admins
  mount RailsAdmin::Engine => '/jogladmin', as: 'rails_admin'

  require 'sidekiq/web'
  require 'sidekiq/cron/web'
  mount Sidekiq::Web => '/sidekiq'

  root 'api/pages#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    root 'pages#index'


#  short_title check
    resources :projects, :communities, :challenges, :programs,
              except: [:index, :create, :show, :update, :destroy],
              :defaults => {:format => :json} do
      collection do
        get  "/exists/:short_title"         => :short_title_exist
      end
    end

    resources :users,
              except: [:index, :create, :show, :update, :destroy],
              :defaults => {:format => :json} do
      collection do
        get "/exists/:nickname"       => :nickname_exist
      end
    end



# Upload Mechanics
    resources :needs, :projects, :challenges, :posts,
              except: [:index, :create, :show, :update, :destroy],
              :defaults => {:format => :json} do
      collection do
        post    "/:id/documents"               => :upload_document
        delete  "/:id/documents/:document_id"  => :remove_document
      end
    end

# Follow Mechanics
    resources :users, :projects, :communities, :challenges, :programs, :needs,
              except: [:index, :create, :show, :update, :destroy],
              :defaults => {:format => :json} do
      collection do
        put   "/:id/follow"           => :follow
        put   "/:id/unfollow"         => :unfollow
        get   "/:id/followers"        => :followers
      end
    end

# Clap Mechanics
    resources :users, :projects, :communities, :challenges, :programs, :posts, :needs,
              except: [:index, :create, :show, :update, :destroy],
              :defaults => {:format => :json} do
      collection do
        put   "/:id/clap"             => :clap
        put   "/:id/unclap"           => :unclap
      end
    end

# Join and Leave Mechanics
    resources :projects, :communities, :challenges, :needs,
              except: [:index, :create, :show, :update, :destroy],
              :defaults => {:format => :json} do
      collection do
        put   "/:id/join"           => :join
        put   "/:id/leave"          => :leave
      end
    end

# Members Mechanics
    resources :projects, :communities, :challenges, :programs, :needs,
              except: [:index, :create, :show, :update, :destroy],
              :defaults => {:format => :json} do
      collection do
        post  "/:id/invite"         => :invite
        post  "/:id/is_member"      => :is_member
        get   "/:id/members"        => :members_list
        post  "/:id/members"        => :update_member
        delete  "/:id/members/:user_id"      => :remove_member
      end
    end

#  Banner
    resources :projects, :communities, :challenges, :programs,
              except: [:index, :create, :show, :update, :destroy],
              :defaults => {:format => :json} do
      collection do
        post  "/:id/banner"         => :upload_banner
        delete "/:id/banner"        => :remove_banner
      end
    end

# Users Specific routes
    resources :users,                   :defaults => { :format => :json } do
      collection do
        get   "/:id/following"        => :following
        post  "/:id/avatar"           => :upload_avatar
        delete "/:id/avatar"          => :remove_avatar
        get   "/:id/:object_type"     => :user_object
        post "/resend_confirmation"   => :resend_confirmation
				post  "/:id/send_email"       => :send_private_email
      end
    end

# Project Specific routes
    resources :projects,                :defaults => { :format => :json } do
      collection do
        get "/mine"                 => :my_projects
        get "/:id/needs"                 => :index_needs
      end
    end

# Needs Specific routes
    resources :needs,                :defaults => { :format => :json } do
      collection do
      end
    end

# communities Specific routes
    resources :communities,               :defaults => {:format => :json} do
      collection do
        get "/mine"                 => :my_communities
      end
    end

# Posts Specific routes
    resources :posts, except: :index,     :defaults => {:format => :json} do
      collection do
        post   "/:id/comment"                => :comment
        patch  "/:id/comment/:comment_id"    => :comment
        delete  "/:id/comment/:comment_id"   => :comment
      end
    end


    resources :feeds, except: [:create, :update, :destroy],     :defaults => {:format => :json} do
      collection do
        # get   "/:id"             => :index
      end
    end


# Challenge Specific routes
    resources :challenges,                :defaults => {:format => :json} do
      collection do
        get   "/:id/projects"               => :index_projects
        post   "/:id/projects/:project_id"  => :set_project_status
        put   "/:id/projects/:project_id"   => :attach
        delete "/:id/projects/:project_id"  => :remove
        get "/mine"                         => :my_challenges
        get "/can_create"                   => :can_create
      end
    end

# Program Specific routes
    resources :programs,               :defaults => {:format => :json} do
      collection do
        get   "/:id/challenges"               => :index_challenges
        put   "/:id/challenges/:challenge_id"   => :attach
        delete "/:id/challenges/:challenge_id"  => :remove
        get "/mine"                 => :my_programs
        get "/getid/:nickname"      => :getid
      end
    end

  end
end
