redis_url = ENV["REDIS_URL"] || 'redis://redis:6379'

# The constant below will represent ONE connection, present globally in models, controllers, views etc for the instance. No need to do Redis.new everytime
REDIS = Redis.new(url: redis_url)
