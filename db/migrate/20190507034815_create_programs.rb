class CreatePrograms < ActiveRecord::Migration[5.2]
  def change
    create_table :programs do |t|
      t.string :description
      t.string :title
      t.datetime :launch_date
      t.datetime :end_date
      t.timestamps
    end

    add_column :challenges, :program_id, :bigint
    add_column :challenges, :launch_date, :datetime
    add_column :challenges, :end_date, :datetime
    add_column :challenges, :final_date, :datetime
    add_column :challenges, :faq, :string
    add_column :posts, :is_need, :boolean

  end
end
