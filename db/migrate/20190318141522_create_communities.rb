class CreateCommunities < ActiveRecord::Migration[5.2]
  def change
    create_table :communities do |t|
      t.string :title
      t.string :short_title
      t.string :banner_url
      t.string :short_description
      t.boolean :is_private
      t.integer :creator_id
      t.integer :feed_id
      t.timestamps
    end
  end
end
