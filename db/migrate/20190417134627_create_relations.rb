class CreateRelations < ActiveRecord::Migration[5.2]
  def change
    create_table :relations do |t|
      t.belongs_to :user
      t.string :resource_type
      t.integer :resource_id
      t.boolean :has_clapped
      t.boolean :follows
      t.timestamps
    end
  end
end
