class Needs < ActiveRecord::Migration[5.2]
  def change
    create_table :needs, force: :cascade do |t|
      t.belongs_to :project, index: true
      t.belongs_to :user, index: true
      t.string :title
      t.string :content
      t.string :status
      t.datetime :end_date
    end

    create_table :users_needs do |t|
      t.belongs_to :user, index: true
      t.belongs_to :need, index: true
    end

    create_table :needs_skills do |t|
      t.belongs_to :need, index: true
      t.belongs_to :skill, index: true
    end

    create_table :needs_projects do |t|
      t.belongs_to :need, index: true
      t.belongs_to :project, index: true
    end

    add_column :feeds, :need_id, :bigint

  end
end
