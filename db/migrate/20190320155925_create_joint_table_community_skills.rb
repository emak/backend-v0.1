class CreateJointTableCommunitySkills < ActiveRecord::Migration[5.2]
  def change
    create_table :communities_skills do |t|
      t.integer :community_id
      t.integer :skill_id
    end
  end
end
