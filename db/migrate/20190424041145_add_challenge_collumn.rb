class AddChallengeCollumn < ActiveRecord::Migration[5.2]
  def change
    add_column :challenges, :short_description, :string
    add_column :challenges, :country, :string
    add_column :challenges, :city, :string
    add_column :challenges, :address, :string
    add_column :challenges, :logo_url, :string
    add_column :challenges, :rules, :string
    add_column :challenges, :latitude, :float
    add_column :challenges, :longitude, :float
    add_column :challenges, :status, :integer
    add_column :feeds, :challenge_id, :bigint
  end

  create_table "challenges_skills", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "skill_id"
  end

  create_table "challenges_interests", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "interest_id"
  end
end
