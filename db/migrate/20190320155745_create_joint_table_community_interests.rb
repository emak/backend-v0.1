class CreateJointTableCommunityInterests < ActiveRecord::Migration[5.2]
  def change
    create_table :communities_interests, index: false do |t|
      t.integer :community_id
      t.integer :interest_id
    end
  end
end
