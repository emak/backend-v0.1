class AddMissingcolumnsToProjComm < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :banner_url, :string
    add_column :communities, :logo_url, :string
    add_column :communities, :description, :string
    add_column :communities, :latitude, :float
    add_column :communities, :longitude, :float
  end
end
