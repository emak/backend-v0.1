class CreateChallengesProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :challenges_projects do |t|
      t.belongs_to :challenge, index: true
      t.belongs_to :project, index: true
      t.string  :status
      t.timestamps
    end
  end
end
