class CreateTableCommunityTags < ActiveRecord::Migration[5.2]
  def change
    create_table :community_tags do |t|
      t.string :tag_name
    end
  end
end
