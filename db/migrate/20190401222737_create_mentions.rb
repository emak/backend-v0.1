class CreateMentions < ActiveRecord::Migration[5.2]
  def change
    create_table :mentions do |t|
      t.belongs_to :post
      t.string :obj_type
      t.integer :obj_id
      t.string :obj_match
      t.timestamps
    end
  end
end
