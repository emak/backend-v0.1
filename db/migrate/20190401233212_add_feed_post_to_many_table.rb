class AddFeedPostToManyTable < ActiveRecord::Migration[5.2]
  def change
    add_index :feeds_posts, [ :feed_id, :post_id ], :unique => true, :name => 'by_feed_and_post'
  end
end
