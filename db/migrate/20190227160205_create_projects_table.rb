class CreateProjectsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :title
      t.string :short_title
      t.string :logo_url
      t.string :description
      t.string :short_description
      t.integer :creator_id
      t.integer :state
      t.float :latitude
      t.float :longitude
      t.integer :feed_id
    end
  end
end
