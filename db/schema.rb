# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_04_135127) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "challenges", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "banner_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "short_description"
    t.string "country"
    t.string "city"
    t.string "address"
    t.string "logo_url"
    t.string "rules"
    t.float "latitude"
    t.float "longitude"
    t.integer "status"
    t.bigint "program_id"
    t.datetime "launch_date"
    t.datetime "end_date"
    t.datetime "final_date"
    t.string "faq"
  end

  create_table "challenges_communities", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "community_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["challenge_id"], name: "index_challenges_communities_on_challenge_id"
    t.index ["community_id"], name: "index_challenges_communities_on_community_id"
  end

  create_table "challenges_interests", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "interest_id"
  end

  create_table "challenges_projects", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "project_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["challenge_id"], name: "index_challenges_projects_on_challenge_id"
    t.index ["project_id"], name: "index_challenges_projects_on_project_id"
  end

  create_table "challenges_skills", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "skill_id"
  end

  create_table "challenges_users", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "user_id"
    t.string "role"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["challenge_id"], name: "index_challenges_users_on_challenge_id"
    t.index ["user_id"], name: "index_challenges_users_on_user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.bigint "post_id"
    t.bigint "user_id"
    t.string "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_comments_on_post_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "communities", force: :cascade do |t|
    t.string "title"
    t.string "short_title"
    t.string "banner_url"
    t.string "short_description"
    t.boolean "is_private"
    t.integer "creator_id"
    t.integer "feed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.string "logo_url"
    t.string "description"
    t.float "latitude"
    t.float "longitude"
    t.string "address"
    t.string "city"
    t.string "country"
  end

  create_table "communities_community_tags", id: false, force: :cascade do |t|
    t.integer "community_tag_id"
    t.integer "community_id"
  end

  create_table "communities_interests", force: :cascade do |t|
    t.integer "community_id"
    t.integer "interest_id"
  end

  create_table "communities_skills", force: :cascade do |t|
    t.integer "community_id"
    t.integer "skill_id"
  end

  create_table "community_tags", force: :cascade do |t|
    t.string "tag_name"
  end

  create_table "enablers", force: :cascade do |t|
    t.string "description"
    t.string "name"
    t.string "organisation_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "feeds", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "project_id"
    t.bigint "community_id"
    t.string "object_type"
    t.bigint "challenge_id"
    t.bigint "program_id"
    t.bigint "need_id"
    t.index ["community_id"], name: "index_feeds_on_community_id"
    t.index ["project_id"], name: "index_feeds_on_project_id"
    t.index ["user_id"], name: "index_feeds_on_user_id"
  end

  create_table "feeds_posts", id: false, force: :cascade do |t|
    t.bigint "feed_id"
    t.bigint "post_id"
    t.index ["feed_id", "post_id"], name: "by_feed_and_post", unique: true
    t.index ["feed_id"], name: "index_feeds_posts_on_feed_id"
    t.index ["post_id"], name: "index_feeds_posts_on_post_id"
  end

  create_table "interests", force: :cascade do |t|
    t.string "interest_name"
  end

  create_table "interests_communities", id: false, force: :cascade do |t|
    t.integer "communities_id"
    t.integer "interest_id"
  end

  create_table "interests_projects", id: false, force: :cascade do |t|
    t.integer "project_id"
    t.integer "interest_id"
  end

  create_table "interests_users", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "interest_id"
  end

  create_table "mentions", force: :cascade do |t|
    t.bigint "post_id"
    t.string "obj_type"
    t.integer "obj_id"
    t.string "obj_match"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_mentions_on_post_id"
  end

  create_table "needs", force: :cascade do |t|
    t.bigint "project_id"
    t.bigint "user_id"
    t.string "title"
    t.string "content"
    t.integer "status"
    t.datetime "end_date"
    t.index ["project_id"], name: "index_needs_on_project_id"
    t.index ["user_id"], name: "index_needs_on_user_id"
  end

  create_table "needs_projects", force: :cascade do |t|
    t.bigint "need_id"
    t.bigint "project_id"
    t.index ["need_id"], name: "index_needs_projects_on_need_id"
    t.index ["project_id"], name: "index_needs_projects_on_project_id"
  end

  create_table "needs_skills", force: :cascade do |t|
    t.bigint "need_id"
    t.bigint "skill_id"
    t.index ["need_id"], name: "index_needs_skills_on_need_id"
    t.index ["skill_id"], name: "index_needs_skills_on_skill_id"
  end

  create_table "posts", force: :cascade do |t|
    t.bigint "feed_id"
    t.bigint "user_id"
    t.string "content"
    t.string "media"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_need"
    t.string "from_object"
    t.bigint "from_id"
    t.string "from_name"
    t.index ["feed_id"], name: "index_posts_on_feed_id"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "programs", force: :cascade do |t|
    t.string "description"
    t.string "title"
    t.datetime "launch_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.string "short_title"
    t.string "faq"
    t.string "enablers"
    t.string "ressources"
    t.string "short_description"
  end

  create_table "projects", force: :cascade do |t|
    t.string "title"
    t.string "short_title"
    t.string "logo_url"
    t.string "description"
    t.string "short_description"
    t.integer "creator_id"
    t.integer "state"
    t.float "latitude"
    t.float "longitude"
    t.integer "feed_id"
    t.integer "status"
    t.string "short_name"
    t.boolean "is_private"
    t.string "banner_url"
    t.string "address"
    t.string "city"
    t.string "country"
  end

  create_table "projects_skills", id: false, force: :cascade do |t|
    t.integer "project_id"
    t.integer "skill_id"
  end

  create_table "relations", force: :cascade do |t|
    t.bigint "user_id"
    t.string "resource_type"
    t.integer "resource_id"
    t.boolean "has_clapped"
    t.boolean "follows"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_relations_on_user_id"
  end

  create_table "relationships", force: :cascade do |t|
    t.integer "follower_id"
    t.integer "followed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["followed_id"], name: "index_relationships_on_followed_id"
    t.index ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
    t.index ["follower_id"], name: "index_relationships_on_follower_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "skills", force: :cascade do |t|
    t.string "skill_name"
  end

  create_table "skills_users", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "skill_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "nickname"
    t.integer "age"
    t.string "social_cat"
    t.string "profession"
    t.string "country"
    t.string "city"
    t.string "address"
    t.string "phone_number"
    t.string "bio"
    t.boolean "allow_password_change"
    t.integer "feed_id"
    t.boolean "email_confirmed", default: false
    t.string "confirm_token"
    t.datetime "locked_at"
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.text "tokens"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "status"
    t.float "latitude"
    t.float "longitude"
    t.string "logo_url"
    t.string "banner_url"
    t.string "affiliation"
    t.string "category"
    t.boolean "mail_newsletter"
    t.boolean "mail_weekly"
    t.string "short_bio"
    t.index ["confirm_token"], name: "index_users_on_confirm_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "users_communities", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "community_id"
    t.string "role"
    t.index ["community_id"], name: "index_users_communities_on_community_id"
    t.index ["user_id"], name: "index_users_communities_on_user_id"
  end

  create_table "users_needs", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "need_id"
    t.index ["need_id"], name: "index_users_needs_on_need_id"
    t.index ["user_id"], name: "index_users_needs_on_user_id"
  end

  create_table "users_programs", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "program_id"
    t.string "role"
    t.string "part"
    t.index ["program_id"], name: "index_users_programs_on_program_id"
    t.index ["user_id"], name: "index_users_programs_on_user_id"
  end

  create_table "users_projects", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "project_id"
    t.string "role"
    t.string "part"
    t.index ["project_id"], name: "index_users_projects_on_project_id"
    t.index ["user_id"], name: "index_users_projects_on_user_id"
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
