require 'rails_helper'


RSpec.describe "ProjectApiUnprotected", type: :request do
  before do
    projects = create_list(:project, 3)
  end

  it "gets the index" do
    path '/api/projects' do
      get(summary: "Get project index") do
        response(200, description:"Get project indexes") do
          body = JSON(response.body)
          expect(body.count).to eq(3)
        end
      end

      get(summary: "test if archived not rendered") do
        before do
          create(:archived_project)
        end

        response(200, description:"Get project indexes") do
          body = JSON(response.body)
          expect(body.count).to eq(3)
        end
      end
    end
  end

  it "gets one project" do
    path '/api/projects/{id}' do
      parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'

      response(200, description: 'Return the selected project') do
        let(:id) { projects[0].id }
        it "is the correct project" do
          body = JSON(response.body)
          expect(body["id"]).to eq(projects[0].id)
        end
      end

      response(404, description: 'Project not found') do
        let(:id) { 999 }
      end

    end
  end
end

RSpec.describe "ProjectApiProtected", type: :request do
  it "creates a project" do
    @user = create(:confirmed_user)

    path '/api/projects' do
      post(summary: "Unauthorized") do
        parameter :data,
                  in: :body,
                  required: true

        response(401, description: 'Unauthorized') do
          let(:data) do
            {
              project:{
                status: "draft",
                title: "A long Title",
                short_title: "AshortTitle",
                logo_url: "https://robohash.org/voluptatumundesed.png?size=300x300",
                description: "A description",
                short_description: "A short description",
                is_private: false,
                interests: [1, 2],
                skills: ["Python", "3D printing"]
              }
            }
          end
        end
      end

      sign_in @user

      path '/api/projects' do
        post(summary: "Unauthorized") do
          parameter :data,
                    in: :body,
                    required: true

          response(201, description: 'Unauthorized') do
            let(:data) do
              {
                project:{
                  status: "draft",
                  title: "A long Title",
                  short_title: "AshortTitle",
                  logo_url: "https://robohash.org/voluptatumundesed.png?size=300x300",
                  description: "A description",
                  short_description: "A short description",
                  is_private: false,
                  interests: [1, 2],
                  skills: ["Python", "3D printing"]
                }
              }
            end
            it "has the right creator_id" do
              body = JSON(response.body)
              expect(body["creator_id"]).to eq(@user.id)
            end
            it "has the right data" do
              body = JSON(response.body)
              expect(body["title"]).to eq("A long Title")
              expect(body["short_title"]).to eq("AshortTitle")
              expect(body["logo_url"]).to eq("https://robohash.org/voluptatumundesed.png?size=300x300")
              expect(body["description"]).to eq("A description")
              expect(body["short_description"]).to eq("A short description")
              expect(body["is_private"]).to eq(false)
              expect(body["interests"]).to eq([1, 2])
              expect(body["skills"]).to eq(["Python", "3D printing"])
            end
          end
        end
      end
    end
  end

  it "can destroy a project" do
    @creator = create(:confirmed_user)
    @project = create(:project, creator_id: @creator.id)

    path '/api/project/{id}' do
      delete
    end
  end

  it "is unauthorized to update a project" do
    @user = create(:user)
    @project = create(:project, creator_id: @user.id)

    path '/api/projects/{id}' do
      patch(summary:"update a project") do
        parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'
        parameter :data,
                  in: :body,
                  required: true
        response(401, description: 'Unauthorized') do
          let(:data) do
            {
              project:{
                status: "draft",
                title: "A long Title",
                short_title: "AshortTitle",
                logo_url: "https://robohash.org/voluptatumundesed.png?size=300x300",
                description: "A description",
                short_description: "A short description",
                is_private: false,
                interests: [1, 2],
                skills: ["Python", "3D printing"]
              }
            }
          end
        end
      end
    end
  end

  it "is forbidden to update a project" do
    @user = create(:user)
    @project = create(:project, creator_id: @user.id + 1)

    sign_in @user

    path '/api/projects/{id}' do
      patch(summary:"Update a project") do
        parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'
        parameter :data,
                  in: :body,
                  required: true
        response(403, description: 'Unauthorized') do
          let(:data) do
            {
              project:{
                status: "draft",
                title: "A long Title",
                short_title: "AshortTitle",
                logo_url: "https://robohash.org/voluptatumundesed.png?size=300x300",
                description: "A description",
                short_description: "A short description",
                is_private: false,
                interests: [1, 2],
                skills: ["Python", "3D printing"]
              }
            }
          end
        end
      end
    end
  end

  it "creator can update project" do
    @user = create(:confirmed_user)
    @project = create(:project, creator_id: @user.id)

    sign_in @user

    path '/api/projects/{id}' do
      patch(summary:"Update a project") do
        parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'
        parameter :data,
                  in: :body,
                  required: true
        response(200, description: 'Success') do
          let(:data) do
            {
              project:{
                status: "draft",
                title: "A long Title",
                short_title: "AshortTitle",
                logo_url: "https://robohash.org/voluptatumundesed.png?size=300x300",
                description: "A description",
                short_description: "A short description",
                is_private: false,
                interests: [1, 2],
                skills: ["Python", "3D printing"]
              }
            }
          end
          it "is updated" do
            body = JSON(response.body)
            expect(body["title"]).to eq("A long Title")
            expect(body["short_title"]).to eq("AshortTitle")
            expect(body["logo_url"]).to eq("https://robohash.org/voluptatumundesed.png?size=300x300")
            expect(body["description"]).to eq("A description")
            expect(body["short_description"]).to eq("A short description")
            expect(body["is_private"]).to eq(false)
            expect(body["interests"]).to eq([1, 2])
            expect(body["skills"]).to eq(["Python", "3D printing"])
          end
        end
      end
    end
  end

  it "admin can update project" do
    @user = create(:confirmed_user)
    @admin = create(:confirmed_user)
    @project = create(:project, creator_id: @user.id)
    @project.users << @admin
    @relation = UsersProject.find_by(project_id: @project.id, user_id: @admin.id)
    @relation.role = 'admin'
    @relation.save!

    sign_in @admin

    path '/api/projects/{id}' do
      patch(summary:"Update the project") do
        parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'
        parameter :data,
                  in: :body,
                  required: true
        response(200, description: 'Success') do
          let(:data) do
            {
              project:{
                status: "draft",
                title: "A long Title",
                short_title: "AshortTitle",
                logo_url: "https://robohash.org/voluptatumundesed.png?size=300x300",
                description: "A description",
                short_description: "A short description",
                is_private: false,
                interests: [1, 2],
                skills: ["Python", "3D printing"]
              }
            }
          end
          it "is updated" do
            body = JSON(response.body)
            expect(body["title"]).to eq("A long Title")
            expect(body["short_title"]).to eq("AshortTitle")
            expect(body["logo_url"]).to eq("https://robohash.org/voluptatumundesed.png?size=300x300")
            expect(body["description"]).to eq("A description")
            expect(body["short_description"]).to eq("A short description")
            expect(body["is_private"]).to eq(false)
            expect(body["interests"]).to eq([1, 2])
            expect(body["skills"]).to eq(["Python", "3D printing"])
          end
        end
      end
    end
  end

  it "can join a project" do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @project = create(:project, creator_id: @creator.id)

    sign_in @member

    path '/api/project/{id}/join' do
      put(summary:"Join project") do
        parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'

        response(200, description: 'Success')

        it "has joined" do
          @project.reload
          expect(@project.users.include?(@member)).to be_truthy
          expect(UsersProject.where(project_id: @project.id, user_id: @member.id).first.role).to eq("member")
        end
      end
    end
  end

  it "can leave a project" do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @project = create(:project, creator_id: @creator.id)
    @project.users << @member

    sign_in @member

    path '/api/project/{id}/leave' do
      put(summary:"Leave the project") do
        parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'

        response(200, description: 'Success')

        it "has leaved" do
          @project.reload
          expect(@project.users.include?(@member)).to be_falsey
        end
      end
    end
  end

  it "can't join a private project" do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @project = create(:private_project, creator_id: @creator.id)

    sign_in @member

    path '/api/project/{id}/join' do
      put(summary:"Join") do
        parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'

        response(200, description: 'Success')

        it "is pending" do
          @project.reload
          expect(@project.users.include?(@member)).to be_truthy
          expect(UsersProject.where(project_id: @project.id, user_id: @member.id).first.role).to eq("pending")
        end
      end
    end
  end

  it "can invite a user" do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @project = create(:private_project, creator_id: @creator.id)

    sign_in @creator

    path '/api/project/{id}/invite' do
      post(summary:"Invite") do
        parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'
        parameter :data,
                  in: :body,
                  required: true

        response(200, description: 'Success') do
          let(:data) do
            {
              user_id: @member.id
            }
          end
          it "is a member" do
            @project.reload
            expect(@project.users.include?(@member)).to be_truthy
            expect(UsersProject.where(project_id: @project.id, user_id: @member.id).first.role).to eq("member")
          end
        end

        response(200, description: 'Success') do
          let(:data) do
            {
              stranger_email: "test@test.com"
            }
          end
        end
      end
    end

    sign_out @creator

    @project.users.delete(@member)

    sign_in @member

    path '/api/project/{id}/invite' do
      post(summary:"Invite") do
        parameter :id, in: :path, type: :integer, required: true, description: 'Project ID'
        parameter :data,
                  in: :body,
                  required: true

        response(403, description: 'forbidden') do
          let(:data) do
            {
              user_id: @member.id
            }
          end
        end

        it "is not a member" do
          @project.reload
          expect(@project.users.include?(@member)).to be_falsey
        end
      end
    end
  end
end
