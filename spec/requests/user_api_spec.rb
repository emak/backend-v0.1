require 'rails_helper'

RSpec.describe "Sessions" do
  it "signs user in and out" do
    user = create(:confirmed_user)    ## uncomment if using FactoryBot
    # user = User.create(email: 'test@test.com', password: "password", password_confirmation: "password") ## uncomment if not using FactoryBot
    sign_in(:user, user)
    get '/api/users/validate_token' do
      response(200, description: 'It is logged in') do
        it 'returned the user' do
          body = JSON(response.body)
          expect(body["data"]["id"]).to eq(user.id)
        end
      end
    end

    sign_out user
    get '/api/users/validate_token' do
      response(401, description: 'It is logged out')
    end
  end
end

RSpec.describe "UsersApiUnprotected", type: :request do
  before do
    users = create_list(:confirmed_user, 3)
  end

  it "gets the index" do
    get '/api/users' do
      response(200, description: 'It gets the index') do
        it 'returned the users' do
          body = JSON(response.body)
          expect(body.count).to eq(3)
        end
      end
    end
  end


  it "gets the user" do
    path '/api/users/{id}' do
      get(summary: "Get one user") do
        consumes 'application/json'
        produces 'application/json'

        parameter :id, in: :path, type: :integer, required: true, description: 'User ID'

        response(200, description: 'Return the selected user') do
          let(:id) { users[0].id }
          it "is the correct user" do
            body = JSON(response.body)
            expect(body["id"]).to eq(users[0].id)
          end
        end

        response(404, description: 'User not found') do
          let(:id) { 999 }
        end
      end
    end
  end

  it "gets the followers" do
    users = create_list(:confirmed_user, 3)
    users[0].following << users[1]
    users[0].following << users[2]
    path '/api/users/{id}/followers' do
      get(summary:"Get followers") do
        let(:id) {users[0].id}
        body = JSON(response.body)
        it "has the correct count" do
          expect(body.count).to eq(2)
        end
      end
    end
  end

  it "gets the followings" do
    users = create_list(:confirmed_user, 3)
    users[0].following << users[1]
    users[0].following << users[2]
    path '/api/users/{id}/following' do
      get(summary:"Get following users") do
        let(:id) {users[1].id}
        body = JSON(response.body)
        it "has the correct count" do
          expect(body.count).to eq(1)
          expect(body.first.id).to eq(users[0].id)
        end
      end
    end
  end
end

RSpec.describe "UsersApiProtected", type: :request do
  before do
    @user_1 = create(:confirmed_user)
    @user_2 = create(:confirmed_user)
  end

  it "creates a user" do
    path '/api/users' do
      post(summary:"Create User") do
        consumes 'application/json'
        produces 'application/json'
        tags :users

        parameter :data,
                  in: :body,
                  required: true

        response(201, description: 'User created') do
          let(:data) do
            {
                user: {
                  email: "mytest@test.com",
                  password: "password",
                  password_confirmation: "password"
                }
            }
          end
          expect(User.where(email: "mytest@test.com").count).to eq(1)
        end

        response(422, description: 'User already exist') do
          before do
            create(:user, email:"mytest@test.com")
          end
          let(:data) do
            {
                user: {
                  email: "mytest@test.com",
                  password: "password",
                  password_confirmation: "password"
                }
            }
          end
        end
      end
    end
  end

  it "is unauthorized to update" do
    path '/api/users{id}' do
      patch(summary: "Update User") do
        consumes 'application/json'
        produces 'application/json'
        tags :users

        parameter :id, in: :path, type: :integer, required: true, description: 'User ID'

        parameter :data,
          in: :body

        response(401, description: "User is not logged in") do
          let(:id) { @user_1.id }
          let(:data) do
            {
              user:
              {
                email: "notauthrorized@email.com"
              }
            }
          end
        end
      end
    end
  end

  it "updates the user" do
    sign_in(:user, @user_1)

    path '/api/users/{id}' do
      patch(summary: "Update User") do
        consumes 'application/json'
        produces 'application/json'
        tags :users

        parameter :id, in: :path, type: :integer, required: true, description: 'User ID'

        parameter :data,
          in: :body

        response(200, description: 'Update the selected user') do
          let(:id) { @user.id }
          let(:data) do
            {
              user:
              {
                email: "thisisatest@email.com",
                first_name: "John",
                last_name: "Doe",
                nickname: "johndoe",
                age: 24,
                social_cat: "Wealthy",
                profession: "Researcher",
                country: "France",
                city: "Paris",
                address: "15 rue de la science",
                phone_number: "+33654637383",
                bio: "This is my bio, it's great!",
                interests: [1, 2],
                skills: ["Python", "3D printing"]
              }
            }
          end
          it "has updated" do
            @user.reload
            expect(@user.email).to eq("thisisatest@email.com")
            expect(@user.first_name).to eq("John")
            expect(@user.last_name).to eq("Doe")
            expect(@user.nickname).to eq("johndoe")
            expect(@user.age).to eq(24)
            expect(@user.social_cat).to eq("Wealthy")
            expect(@user.profession).to eq("Researcher")
            expect(@user.country).to eq("France")
            expect(@user.city).to eq("Paris")
            expect(@user.address).to eq("15 rue de la science")
            expect(@user.phone_number).to eq("+33654637383")
            expect(@user.bio).to eq("This is my bio, it's great!")
            expect(@user.interests.first.id).to eq(1)
            expect(@user.interests.last.id).to eq(2)
            expect(@user.skills.as_json.count()).to eq(2)
          end
        end

        response(403, description: "Can't update other user") do
          let(:id) { @user_2.id }
          let(:data) do
            {
              user:
              {
                email: "notauthrorized@email.com"
              }
            }
          end
        end
      end
    end
  end

  it "archives the user" do
    path '/api/users/{id}' do
      delete(summary:"Delete while not logged in") do
        parameter :id, in: :path, type: :integer, required: true, description: 'User ID'
        response(401, description: "unauthorized") do
          let(:id) { @user_1.id }
        end
      end
    end

    sign_in(:user, @user_1)

    path '/api/users/{id}' do
      delete(summary:"Delete while not logged in") do
        parameter :id, in: :path, type: :integer, required: true, description: 'User ID'
        response(200, description: "unauthorized") do
          let(:id) { @user_1.id }
        end
        it "is archved" do
          @user_1.reload
          expect(@user_1.status).to eq("archived")
        end
      end
    end
  end

  it "follows someone" do
    path '/api/users/{id}/follow' do
      put(summary:"follow while not logged in") do
        parameter :id, in: :path, type: :integer, required: true, description: 'User ID'
        response(401, description: "unauthorized") do
          let(:id) { @user_2.id }
        end
      end
    end

    sign_in @user_1

    path '/api/users/{id}/follow' do
      put(summary:"follow user") do
        parameter :id, in: :path, type: :integer, required: true, description: 'User ID'
        response(200, description: "ok") do
          let(:id) { @user_2.id }
        end
        it "is following" do
          @user_1.reload
          expect(@user_1.following.first.id).to eq(@user_2.id)
        end
        it "match the follower" do
          @user_2.reload
          expect(@user_2.followers.first.id).to eq(@user_1.id)
        end
      end
    end
  end

  it "unfollows someone" do
    path '/api/users/{id}/unfollow' do
      put(summary:"unfollow while not logged in") do
        parameter :id, in: :path, type: :integer, required: true, description: 'User ID'
        response(401, description: "unauthorized") do
          let(:id) { @user_2.id }
        end
      end
    end

    sign_in(:user, @user_1)

    @user_1.following << @user_2

    path '/api/users/{id}/unfollow' do
      put(summary:"unfollow user") do
        parameter :id, in: :path, type: :integer, required: true, description: 'User ID'
        response(200, description: "ok") do
          let(:id) { @user_2.id }
        end
        it "is not following" do
          @user_1.reload
          expect(@user_1.following.count).to eq(0)
        end
        it "match the follower" do
          @user_2.reload
          expect(@user_2.followers.count).to eq(0)
        end
      end
    end
  end
end
