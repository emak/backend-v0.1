require 'rails_helper'

RSpec.describe "Feed" do

  it "renders a public feed" do
    @user = create(:confirmed_user)
    @project = create(:project)
    @feed = @user.feed
    @post_1 = create(:post, user_id: @user.id, feed_id: @feed.id)
    @feed.posts << @post_1
    @post_2 = create(:post, user_id: @user.id, feed_id: @feed.id)
    @feed.posts << @post_2
    @mention = create(:mention, post_id: @post_1.id, obj_id: @project.id, obj_type: 'project', obj_match: '#' + @project.short_title)
    @project.feed.posts << @post_1

    path '/api/feed/{id}' do
      parameter :id, in: :path, type: :integer, required: true, description: 'Feed ID'
      get(summary:"Get a feed") do
        let(:id) {@feed.id}
        response(200, description: "It gets the feed") do
          body = JSON(response.body)
          expect(body.count).to eq(2)
          expect(body[0]['id']).to eq(@post_1.id)
          expect(body[0]['content']).to eq(@post_1.content)
          expect(body[0]['media']).to eq(@post_1.media)
          expect(body[0]['creator']['user_id']).to eq(@user.id)
          expect(body[0]['claps']).to eq(@post_1.claps)
          expect(body[0]['mentions'].count).to eq(0)
          expect(body[1]['id']).to eq(@post_2.id)
          expect(body[1]['content']).to eq(@post_2.content)
          expect(body[1]['media']).to eq(@post_2.media)
          expect(body[1]['creator']['user_id']).to eq(@user.id)
          expect(body[1]['claps']).to eq(@post_2.claps)
          expect(body[1]['mentions'].count).to eq(1)
          expect(body[1]['mentions']["obj_id"]).to eq(@mention.obj_id)
          expect(body[1]['mentions']["obj_type"]).to eq(@mention.obj_type)
          expect(body[1]['mentions']["obj_match"]).to eq(@mention.obj_match)
        end
      end
    end

    path '/api/feed' do
      get(summary:"Get a feed") do
        response(401, description: "Unauthorized")
      end
    end

    sign_in @user

    path '/api/feed' do
      get(summary:"Get a feed") do
        response(200, description: "It gets the feed") do
          body = JSON(response.body)
          expect(body.count).to eq(2)
          expect(body[0]['id']).to eq(@post_1.id)
          expect(body[0]['content']).to eq(@post_1.content)
          expect(body[0]['media']).to eq(@post_1.media)
          expect(body[0]['creator']['user_id']).to eq(@user.id)
          expect(body[0]['claps']).to eq(@post_1.claps)
          expect(body[0]['mentions'].count).to eq(0)
          expect(body[1]['id']).to eq(@post_2.id)
          expect(body[1]['content']).to eq(@post_2.content)
          expect(body[1]['media']).to eq(@post_2.media)
          expect(body[1]['creator']['user_id']).to eq(@user.id)
          expect(body[1]['claps']).to eq(@post_2.claps)
          expect(body[1]['mentions'].count).to eq(1)
          expect(body[1]['mentions']["obj_id"]).to eq(@mention.obj_id)
          expect(body[1]['mentions']["obj_type"]).to eq(@mention.obj_type)
          expect(body[1]['mentions']["obj_match"]).to eq(@mention.obj_match)
        end
      end
    end
  end
end
