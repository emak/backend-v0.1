require "rails_helper"
RSpec.describe User, :type => :model do

  before(:all) do
    @user1 = create(:user)
  end

  it "needs confirmation" do
    expect(@user1.confirmed?).to be false
  end

  it "is confirmed" do
    @user1.confirm
    expect(@user1.confirmed?).to be true
  end

  it "is valid with valid attributes" do
    expect(@user1).to be_valid
  end

  it "has a unique email" do
    user2 = build(:user, email: @user1.email)
    expect(user2).to_not be_valid
  end
  # TODO must only work on update not create
  # it "has a unique nickname" do
  #   user2 = build(:user, nickname: @user1.nickname)
  #   expect(user2).to_not be_valid
  # end

  #TODO Must have a feed

  it "is not valid without a password" do
    user2 = build(:user, password: nil)
    expect(user2).to_not be_valid
  end

  it "is not valid without an email" do
    user2 = build(:user, email: nil)
    expect(user2).to_not be_valid
  end
end
