require 'ffaker'

FactoryBot.define do
  factory :project do

    title {FFaker::Movie.title}
    short_title {FFaker::Internet.user_name}
    logo_url {FFaker::Avatar.image}
    description {FFaker::DizzleIpsum.paragraphs}
    short_description {FFaker::DizzleIpsum.paragraph}

    latitude { rand(-90.000000000...90.000000000) }
    longitude { rand(-180.000000000...180.000000000) }

    # Feed.create({project_id: self.id, object_type:'project'})

    status { 2 }
    is_private {false}
  end

  factory :public_project, :parent => :project do
    is_private {false}
  end

  factory :archived_project, :parent => :project do
    status { 1 }
  end

  factory :private_project, :parent => :project do
    is_private {true}
  end
end
