require 'ffaker'

FactoryBot.define do
  factory :user, aliases: [:creator] do

    email {FFaker::Internet.free_email}
    first_name {FFaker::Name.first_name}
    last_name {FFaker::Name.last_name}
    logo_url {FFaker::Avatar.image}
    nickname {FFaker::Internet.user_name}
    password { "password" }
    password_confirmation { "password" }
    age { rand(56)}
    profession {FFaker::Job.title}
    social_cat {FFaker::Education.degree}
    country {FFaker::Address.country}
    city {FFaker::Address.city}
    address {FFaker::Address.street_address}
    bio {FFaker::DizzleIpsum.sentence}

  end

  factory :confirmed_user, :parent => :user do
    after(:create) { |user| user.confirm }
  end
end
