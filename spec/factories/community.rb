require 'ffaker'

FactoryBot.define do
  factory :community do

    title {FFaker::Movie.title}
    short_title {FFaker::Internet.user_name}
    logo_url {FFaker::Avatar.image}
    description {FFaker::DizzleIpsum.paragraphs}
    short_description {FFaker::DizzleIpsum.paragraph}

    latitude { rand(-90.000000000...90.000000000) }
    longitude { rand(-180.000000000...180.000000000) }

    status { 0 }
    is_private {true}
  end

  factory :public_community, :parent => :community do
    is_private {false}
  end

  factory :private_community, :parent => :community do
    is_private {true}
  end

  factory :archived_community, :parent => :project do
    status { 1 }
  end
end
