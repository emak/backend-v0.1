require 'ffaker'

FactoryBot.define do
  factory :interest do
    interest_name { FFaker::Movie.title }
  end
end
