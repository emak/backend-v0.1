FactoryBot.define do
  factory :comment do
    # Needs to give a parameter as
    # post_id: ID of an existing post

    content {FFaker::DizzleIpsum.paragraphs}
  end
end
