FactoryBot.define do
  factory :post do

    # Need to set 2 parameters !
    # feed_id: ID of an existing feed
    # user_id: id of an existing user
    media {FFaker::Avatar.image}
    content {FFaker::DizzleIpsum.paragraphs}
  end
end
